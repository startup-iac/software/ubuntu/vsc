#!/bin/bash

# Visual Studio Code
wget -qO - https://packages.microsoft.com/keys/microsoft.asc | sudo tee -a $KEYRINGS_PATH/microsoft-keyring.asc
echo \
    "deb [arch=$(dpkg --print-architecture) signed-by=$KEYRINGS_PATH/microsoft-keyring.asc] \
    https://packages.microsoft.com/repos/vscode stable main" \
    | sudo tee /etc/apt/sources.list.d/microsoft.list


# Update Repositories
sudo apt-get update

# Install Visual Studio Code
sudo apt-get install -y code
